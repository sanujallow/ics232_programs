   1              		.file	"findmax.c"
   2              		.intel_syntax noprefix
   3              		.text
   4              	.Ltext0:
   5              		.data
   6              		.align 16
   9              	array1:
  10 0000 01000000 		.long	1			/*integers in contiguous memory locations, 4 bytes apart- */
  11 0004 FFFFFFFF 		.long	-1
  12 0008 24000000 		.long	36
  13 000c 20000000 		.long	32
  14 0010 40000000 		.long	64
  15 0014 40000000 		.long	64
  16 0018 9FFFFFFF 		.long	-97
  17              		.section	.rodata
  18              	.LC0:
  19 0000 4D617820 		.string	"Max value for array1 is: %d\n"
  19      76616C75 
  19      6520666F 
  19      72206172 
  19      72617931 
  20              		.text
  21              		.globl	main
  23              	main:
  24              	.LFB0:
  25              		.file 1 "findmax.c"
   1:findmax.c     **** #include <stdio.h>
   2:findmax.c     **** 
   3:findmax.c     **** /*
   4:findmax.c     **** @Author: Momodou Jallow
   5:findmax.c     **** Metropolitan State University
   6:findmax.c     **** ICS 232 Computer Organization and Architecture
   7:findmax.c     **** Fall 2021
   8:findmax.c     **** 
   9:findmax.c     **** A program to find maximum value in a given array of integers
  10:findmax.c     **** */
  11:findmax.c     **** 
  12:findmax.c     **** //integer arrays
  13:findmax.c     **** static int array1[] = {1, -1, 36, 32, 64, 64, -97};
  14:findmax.c     **** // static int array2[] = {-215, 1, -10, 50, -40, 16};
  15:findmax.c     **** 
  16:findmax.c     **** //function declarations
  17:findmax.c     **** int findMax(int arr[], int len);
  18:findmax.c     **** void printArrays(int arr[], int len); //helper function
  19:findmax.c     **** 
  20:findmax.c     **** //main function
  21:findmax.c     **** int main(void)
  22:findmax.c     **** {
  26              		.loc 1 22 1
  27              		.cfi_startproc
  28 0000 F30F1EFA 		endbr64
  29 0004 55       		push	rbp
  30              		.cfi_def_cfa_offset 16
  31              		.cfi_offset 6, -16
  32 0005 4889E5   		mov	rbp, rsp
  33              		.cfi_def_cfa_register 6
  34 0008 4883EC10 		sub	rsp, 16					;/* 16 bytes reserved for local variables on the stack lower addresses */
  23:findmax.c     ****     int length_of_array = sizeof(array1) / sizeof(array1[0]);
  35              		.loc 1 23 9
  36 000c C745F807 		mov	DWORD PTR -8[rbp], 7	;/*num 7, the length of array is pushed o stack frame */
  36      000000
  24:findmax.c     **** 
  25:findmax.c     ****     //visual inspection
  26:findmax.c     ****     printArrays(array1, length_of_array);
  37              		.loc 1 26 5
  38 0013 8B45F8   		mov	eax, DWORD PTR -8[rbp]		;/*value at pointer to location -8[rbp] is moved to our eax register, and */
  39 0016 89C6     		mov	esi, eax					;/*eax moved to esi(stack index) since it is a function argument */
  40 0018 488D3D00 		lea	rdi, array1[rip] 			;/*Can't fully explain this one, but RIP (Instruction Pointer Register) - location of array[rip] set as effective address of array*/
  40      000000
  41 001f E8000000 		call	printArrays
  41      00
  27:findmax.c     **** 
  28:findmax.c     ****     //find max for each array
  29:findmax.c     ****     int arr1MaxVal = findMax(array1, length_of_array); /*...no longer used*/
  42              		.loc 1 29 22
  43 0024 8B45F8   		mov	eax, DWORD PTR -8[rbp]	;	
  44 0027 89C6     		mov	esi, eax					
  45 0029 488D3D00 		lea	rdi, array1[rip]			
  45      000000
  46 0030 E8000000 		call	findMax					;/*Call to findMax function*/
  46      00
  47 0035 8945FC   		mov	DWORD PTR -4[rbp], eax
  30:findmax.c     **** 
  31:findmax.c     ****     printf("Max value for array1 is: %d\n", arr1MaxVal);
  48              		.loc 1 31 5
  49 0038 8B45FC   		mov	eax, DWORD PTR -4[rbp]		
  50 003b 89C6     		mov	esi, eax
  51 003d 488D3D00 		lea	rdi, .LC0[rip]
  51      000000
  52 0044 B8000000 		mov	eax, 0
  52      00
  53 0049 E8000000 		call	printf@PLT
  53      00
  54 004e B8000000 		mov	eax, 0
  54      00
  32:findmax.c     **** }
  55              		.loc 1 32 1
  56 0053 C9       		leave
  57              		.cfi_def_cfa 7, 8	
  58 0054 C3       		ret
  59              		.cfi_endproc
  60              	.LFE0:
  62              		.globl	findMax
  64              	findMax:
  65              	.LFB1:
  33:findmax.c     **** 
  34:findmax.c     **** /*findMax function */
  35:findmax.c     **** int findMax(int arr[], int len)
  36:findmax.c     **** {
  66              		.loc 1 36 1
  67              		.cfi_startproc
  68 0055 F30F1EFA 		endbr64
  69 0059 55       		push	rbp
  70              		.cfi_def_cfa_offset 16
  71              		.cfi_offset 6, -16
  72 005a 4889E5   		mov	rbp, rsp
  73              		.cfi_def_cfa_register 6
  74 005d 48897DE8 		mov	QWORD PTR -24[rbp], rdi
  75 0061 8975E4   		mov	DWORD PTR -28[rbp], esi
  37:findmax.c     ****     //A function to find the maximum value of an array of integers
  38:findmax.c     ****     //parameters: array, len (length of array)
  39:findmax.c     ****     //returns max value
  40:findmax.c     **** 
  41:findmax.c     ****     int max = arr[0]; //initialize first element as max
  76              		.loc 1 41 9
  77 0064 488B45E8 		mov	rax, QWORD PTR -24[rbp]
  78 0068 8B00     		mov	eax, DWORD PTR [rax]
  79 006a 8945F8   		mov	DWORD PTR -8[rbp], eax
  80              	.LBB2:
  42:findmax.c     **** 
  43:findmax.c     ****     for (int i = 1; i < len; i++)
  81              		.loc 1 43 14
  82 006d C745FC01 		mov	DWORD PTR -4[rbp], 1
  82      000000
  83              		.loc 1 43 5
  84 0074 EB38     		jmp	.L4
  85              	.L6:
  44:findmax.c     ****     {
  45:findmax.c     ****         if (arr[i] > max)
  86              		.loc 1 45 16
  87 0076 8B45FC   		mov	eax, DWORD PTR -4[rbp]
  88 0079 4898     		cdqe
  89 007b 488D1485 		lea	rdx, 0[0+rax*4]
  89      00000000 
  90 0083 488B45E8 		mov	rax, QWORD PTR -24[rbp]
  91 0087 4801D0   		add	rax, rdx
  92 008a 8B00     		mov	eax, DWORD PTR [rax]
  93              		.loc 1 45 12
  94 008c 3945F8   		cmp	DWORD PTR -8[rbp], eax
  95 008f 7D19     		jge	.L5
  46:findmax.c     ****         {
  47:findmax.c     ****             max = arr[i];
  96              		.loc 1 47 22
  97 0091 8B45FC   		mov	eax, DWORD PTR -4[rbp]
  98 0094 4898     		cdqe
  99 0096 488D1485 		lea	rdx, 0[0+rax*4]
  99      00000000 
 100 009e 488B45E8 		mov	rax, QWORD PTR -24[rbp]
 101 00a2 4801D0   		add	rax, rdx
 102              		.loc 1 47 17
 103 00a5 8B00     		mov	eax, DWORD PTR [rax]
 104 00a7 8945F8   		mov	DWORD PTR -8[rbp], eax
 105              	.L5:
  43:findmax.c     ****     {
 106              		.loc 1 43 31 discriminator 2
 107 00aa 8345FC01 		add	DWORD PTR -4[rbp], 1
 108              	.L4:
  43:findmax.c     ****     {
 109              		.loc 1 43 5 discriminator 1
 110 00ae 8B45FC   		mov	eax, DWORD PTR -4[rbp]
 111 00b1 3B45E4   		cmp	eax, DWORD PTR -28[rbp]
 112 00b4 7CC0     		jl	.L6
 113              	.LBE2:
  48:findmax.c     ****         }
  49:findmax.c     ****     }
  50:findmax.c     **** 
  51:findmax.c     ****     return max;
 114              		.loc 1 51 12
 115 00b6 8B45F8   		mov	eax, DWORD PTR -8[rbp]
  52:findmax.c     **** }
 116              		.loc 1 52 1
 117 00b9 5D       		pop	rbp
 118              		.cfi_def_cfa 7, 8
 119 00ba C3       		ret
 120              		.cfi_endproc
 121              	.LFE1:
 123              		.section	.rodata
 124              	.LC1:
 125 001d 61727261 		.string	"array: [\t"
 125      793A205B 
 125      0900
 126              	.LC2:
 127 0027 25640900 		.string	"%d\t"
 128              	.LC3:
 129 002b 5D00     		.string	"]"
 130              		.text
 131              		.globl	printArrays
 133              	printArrays:
 134              	.LFB2:
  53:findmax.c     **** void printArrays(int arr[], int len)
  54:findmax.c     **** {
 135              		.loc 1 54 1
 136              		.cfi_startproc
 137 00bb F30F1EFA 		endbr64
 138 00bf 55       		push	rbp
 139              		.cfi_def_cfa_offset 16
 140              		.cfi_offset 6, -16
 141 00c0 4889E5   		mov	rbp, rsp
 142              		.cfi_def_cfa_register 6
 143 00c3 4883EC20 		sub	rsp, 32
 144 00c7 48897DE8 		mov	QWORD PTR -24[rbp], rdi
 145 00cb 8975E4   		mov	DWORD PTR -28[rbp], esi
  55:findmax.c     ****     //a function to printArrays both arrays on the console
  56:findmax.c     ****     printf("array: [\t");
 146              		.loc 1 56 5
 147 00ce 488D3D00 		lea	rdi, .LC1[rip]
 147      000000
 148 00d5 B8000000 		mov	eax, 0
 148      00
 149 00da E8000000 		call	printf@PLT
 149      00
 150              	.LBB3:
  57:findmax.c     ****     for (int i = 0; i < len; i++)
 151              		.loc 1 57 14
 152 00df C745FC00 		mov	DWORD PTR -4[rbp], 0
 152      000000
 153              		.loc 1 57 5
 154 00e6 EB2D     		jmp	.L9
 155              	.L10:
  58:findmax.c     ****     {
  59:findmax.c     ****         printf("%d\t", arr[i]);
 156              		.loc 1 59 27 discriminator 3
 157 00e8 8B45FC   		mov	eax, DWORD PTR -4[rbp]
 158 00eb 4898     		cdqe
 159 00ed 488D1485 		lea	rdx, 0[0+rax*4]
 159      00000000 
 160 00f5 488B45E8 		mov	rax, QWORD PTR -24[rbp]
 161 00f9 4801D0   		add	rax, rdx
 162              		.loc 1 59 9 discriminator 3
 163 00fc 8B00     		mov	eax, DWORD PTR [rax]
 164 00fe 89C6     		mov	esi, eax
 165 0100 488D3D00 		lea	rdi, .LC2[rip]
 165      000000
 166 0107 B8000000 		mov	eax, 0
 166      00
 167 010c E8000000 		call	printf@PLT
 167      00
  57:findmax.c     ****     for (int i = 0; i < len; i++)
 168              		.loc 1 57 31 discriminator 3
 169 0111 8345FC01 		add	DWORD PTR -4[rbp], 1
 170              	.L9:
  57:findmax.c     ****     for (int i = 0; i < len; i++)
 171              		.loc 1 57 5 discriminator 1
 172 0115 8B45FC   		mov	eax, DWORD PTR -4[rbp]
 173 0118 3B45E4   		cmp	eax, DWORD PTR -28[rbp]
 174 011b 7CCB     		jl	.L10
 175              	.LBE3:
  60:findmax.c     ****     }
  61:findmax.c     ****     printf("]\n");
 176              		.loc 1 61 5
 177 011d 488D3D00 		lea	rdi, .LC3[rip]
 177      000000
 178 0124 E8000000 		call	puts@PLT
 178      00
  62:findmax.c     **** }...
 179              		.loc 1 62 1
 180 0129 90       		nop
 181 012a C9       		leave
 182              		.cfi_def_cfa 7, 8
 183 012b C3       		ret
 184              		.cfi_endproc
 185              	.LFE2:
 187              	.Letext0:
 188              		.file 2 "/usr/lib/gcc/x86_64-linux-gnu/9/include/stddef.h"
 189              		.file 3 "/usr/include/x86_64-linux-gnu/bits/types.h"
 190              		.file 4 "/usr/include/x86_64-linux-gnu/bits/types/struct_FILE.h"
 191              		.file 5 "/usr/include/x86_64-linux-gnu/bits/types/FILE.h"
 192              		.file 6 "/usr/include/stdio.h"
 193              		.file 7 "/usr/include/x86_64-linux-gnu/bits/sys_errlist.h"
 1174              		.section	.note.gnu.property,"a"
 1175              		.align 8
 1176 0000 04000000 		.long	 1f - 0f
 1177 0004 10000000 		.long	 4f - 1f
 1178 0008 05000000 		.long	 5
 1179              	0:
 1180 000c 474E5500 		.string	 "GNU"
 1181              	1:
 1182              		.align 8
 1183 0010 020000C0 		.long	 0xc0000002
 1184 0014 04000000 		.long	 3f - 2f
 1185              	2:
 1186 0018 03000000 		.long	 0x3
 1187              	3:
 1188 001c 00000000 		.align 8
 1189              	4:
