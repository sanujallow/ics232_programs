#include <stdio.h>

/*
@Author: Momodou Jallow
Metropolitan State University
ICS 232 Computer Organization and Architecture
Fall 2021

A program to find maximum value in a given array of integers
*/

//integer arrays
static int array1[] = {1, -1, 36, 32, 64, 64, -97};
// static int array2[] = {-215, 1, -10, 50, -40, 16};

//function declarations
int findMax(int arr[], int len);
void printArrays(int arr[], int len); //helper function

//main function
int main(void)
{
    int length_of_array = sizeof(array1) / sizeof(array1[0]);

    //visual inspection
    printArrays(array1, length_of_array);

    //find max for each array
    int arr1MaxVal = findMax(array1, length_of_array);

    printf("Max value for array1 is: %d\n", arr1MaxVal);
}

/*findMax function */
int findMax(int arr[], int len)
{
    //A function to find the maximum value of an array of integers
    //parameters: array, len (length of array)
    //returns max value

    int max = arr[0]; //initialize first element as max

    for (int i = 1; i < len; i++)
    {
        if (arr[i] > max)
        {
            max = arr[i];
        }
    }

    return max;
}
void printArrays(int arr[], int len)
{
    //a function to printArrays both arrays on the console
    printf("array: [\t");
    for (int i = 0; i < len; i++)
    {
        printf("%d\t", arr[i]);
    }
    printf("]\n");
}